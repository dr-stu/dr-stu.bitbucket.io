window.midiTool = window.midiTool || {};
window.midiTool.midi = {};

((exports) => {


  exports = {
    MIDI_MESSAGES,
    midiInputDevices,
    midiOutputDevice,
    addMidiMessageListeners,
    sendMidiProgramChange,
    sendMidiControlChange,
  };

})(window.midiTool.midi);
