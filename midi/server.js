// node server.js
// node ../app-gateway/node_modules/nodemon/bin/nodemon.js server.js
// http://localhost:3999/Pages/UserProfile/GetData

const http = require('http');
const fs = require('fs');

http.createServer(getResponse).listen(4321);

const extensionContentTypes = {
  css: 'text/css',
  csv: 'text/csv',
  html: 'text/html',
  js: 'application/javascript',
  json: 'application/json',
  png: 'image/png',
  text: 'text/plain',
};

const defaultResponseProps = {
  path: 'not-found',
  status: 200,
  contentType: extensionContentTypes.html,
  delay: 0,
};

function getResponse(req, res) {
  const responseProps = {
    ...defaultResponseProps,
    ...getMockPath(req),
  };

  const extensionMatch = responseProps.path.match(/\.([^\.\/]+)$/);
  if (extensionMatch) {
    responseProps.contentType = extensionContentTypes[extensionMatch[1]];
  }

  fs.readFile(responseProps.path, 'utf8', (err, data) => {
    setTimeout(() => {
      respond(res, err, data, responseProps);
    }, (responseProps.delay * 1000));
  });
}

function respond(res, err, data, props) {
  const { path, status, contentType } = props;
  if (err) {
    res.writeHead(500, { 'Content-Type': extensionContentTypes.text });
    console.error('ERROR:', err);
    res.write(err.toString());
  } else {
    res.writeHead(status, { 'Content-Type': contentType });
    console.log(`${status} ${path} (${data && data.length || 0} bytes, ${contentType})`);
    res.write(data);
  }
  res.end();
}

function getMockPath(req) {
  console.log(':::: ', req.url);
  // if (req.url === '/Pages/UserProfile/GetData') {
  //   console.log(' - - - - - - - - - - - - - - - ');
  //   return {
  //     status: 200,
  //     path: './userProfile.json'
  //   };
  // }

  return {
    path: req.url
  };
}
